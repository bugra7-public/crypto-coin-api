package com.dobra.coin.controller

import com.dobra.coin.model.CoinMarketResponseModel
import com.dobra.coin.model.CryptoCurrencyModel
import com.dobra.coin.model.PriceSupplyRatioModel
import com.dobra.coin.model.QuoteModel
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.math.BigDecimal
import java.math.RoundingMode

@RestController
@RequestMapping("coin-supply")
class CoinSupplyController {

    private val symbolUSD = "USD"
    private val sortTypeMaxSupply = "maxSupply"

    @GetMapping
    private fun getSupplyPriceRatios(
        @RequestParam coinCount: String,
        @RequestParam sortBy: String
    ): MutableList<PriceSupplyRatioModel> {
        val priceSupplyRatioList: MutableList<PriceSupplyRatioModel> = mutableListOf()

        val restTemplate = RestTemplate()

        val apiResponse = restTemplate.getForEntity(getApiAddress(coinCount), CoinMarketResponseModel::class.java)

        val apiCoins = apiResponse.body?.data?.cryptoCurrencyList

        if (apiCoins != null) {
            for (apiCoin in apiCoins) {
                val price = findPrice(apiCoin.quotes)

                val maxSupply = findMaxSupply(apiCoin)

                val priceSupplyRatioModel = PriceSupplyRatioModel(
                    0,
                    apiCoin.name,
                    apiCoin.symbol,
                    findPriceSupplyRatio(price, apiCoin.circulatingSupply),
                    findPriceSupplyRatio(price, maxSupply)
                )

                priceSupplyRatioList.add(priceSupplyRatioModel)
            }
        }

        var placeNo = 1

        return priceSupplyRatioList.apply {
            sortBy {
                if (sortBy == sortTypeMaxSupply) {
                    it.priceOverMaxSupply
                } else {
                    it.priceOverCurrentSupply
                }
            }
        }.onEach {
            it.place = placeNo
            placeNo++
        }
    }

    private fun getApiAddress(coinCount: String): String {
        return "https://api.coinmarketcap.com/data-api/v3/cryptocurrency/listing?start=1&limit=$coinCount&sortBy=market_cap&sortType=desc&convert=USD&cryptoType=all&tagType=all&audited=false&aux=max_supply,circulating_supply,total_supply"
    }

    private fun findPrice(quotes: List<QuoteModel>): BigDecimal {
        return quotes.first {
            it.name == symbolUSD
        }.price
    }

    private fun findMaxSupply(apiCoin: CryptoCurrencyModel): BigDecimal {
        return if (apiCoin.maxSupply != null && apiCoin.maxSupply.setScale(
                0,
                RoundingMode.HALF_UP
            ) != BigDecimal.ZERO
        ) apiCoin.maxSupply
        else if (apiCoin.totalSupply != null && apiCoin.totalSupply.setScale(
                0,
                RoundingMode.HALF_UP
            ) != BigDecimal.ZERO
        ) apiCoin.totalSupply
        else apiCoin.circulatingSupply
    }

    private fun findPriceSupplyRatio(price: BigDecimal, circulatingSupply: BigDecimal): BigDecimal {
        return price.divide(circulatingSupply, RoundingMode.HALF_UP)
    }

}