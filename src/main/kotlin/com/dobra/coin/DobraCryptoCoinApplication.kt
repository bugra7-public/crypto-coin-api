package com.dobra.coin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DobraCryptoCoinApplication

fun main(args: Array<String>) {
	runApplication<DobraCryptoCoinApplication>(*args)
}
