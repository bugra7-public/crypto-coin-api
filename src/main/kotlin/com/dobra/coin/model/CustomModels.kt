package com.dobra.coin.model

import java.math.BigDecimal

data class PriceSupplyRatioModel(
    var place: Int,
    val name: String,
    val symbol: String,
    val priceOverCurrentSupply: BigDecimal,
    val priceOverMaxSupply: BigDecimal
)
