package com.dobra.coin.model

import java.io.Serializable
import java.math.BigDecimal

data class CoinMarketResponseModel(
    val data: CoinMarketResponseDataModel
) : Serializable

data class CoinMarketResponseDataModel(
    val cryptoCurrencyList: List<CryptoCurrencyModel>
) : Serializable

data class CryptoCurrencyModel(
    val name: String,
    val symbol: String,
    val circulatingSupply: BigDecimal,
    val totalSupply: BigDecimal?,
    val maxSupply: BigDecimal?,
    val quotes: List<QuoteModel>
) : Serializable

data class QuoteModel(
    val name: String,
    val price: BigDecimal
) : Serializable